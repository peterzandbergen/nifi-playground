# Nifi Sample Notes

Running Nifi in Docker to test retrieving files from 
GCS and Azure and put it in a local directory.

## Docker commands

### Start Nifi

```
docker run --name nifi \
    -v /home/peza/DevProjects/nifi/data:/tmp/peza/nifi/data \
    -p 8080:8080 \
    apache/nifi:latest
```

## Docker Compose commands

Use `docker-compose` to start multiple services in one go.

### Create and start two nifi nodes and the registry

```
docker-compose up 
```

### Stop all services

```
docker-compose stop
```

### Start all services

```
docker-compose start
```

### Start named services

```
docker-compose start nifi nifi-registry
```

